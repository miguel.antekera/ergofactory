$(document).ready(function() {

    // Force autoplay home video
    var $video = $('#bgvid');
        $video.on('canplaythrough', function() {
        this.play();
    });

    // Slide home
    $('.slide-home').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        arrows: false,
        fade: true,
    });

    // wrap select
    $(".custom-select").wrap("<div class='custom-select'></div>");
    $(".entry video, .bg-blue-dark video, .entry iframe").wrap("<div class='embed-responsive embed-responsive-16by9'></div>");

    // Checkout page

    $(".checkout-form").removeClass("hide");
    $(".checkout-form .text-field label, .checkout-form .select-field label, .checkout-form .textarea-field label").wrap("<div class='col-sm-3'></div>");
    $(".checkout-form .text-field input, .checkout-form select, .checkout-form .textarea").wrap("<div class='col-sm-9'></div>");
    $(".checkout-form #order_customer_email").attr("placeholder", "Ingresa tu E-mail");
    $(".checkout-form #order_customer_phone").attr("placeholder", "Ingresa tu teléfono");
    $(".checkout-form #order_contact_empresa").attr("placeholder", "Nombre de la empresa");
    $('.checkout-form #order_contact_como_llegaste').find('option:first').text('¿Cómo te enteraste de Ergofactory?');
    $('.checkout-form #order_shipping_address_name, .checkout-form #order_billing_address_name').attr("placeholder", "Ingresa tu nombre");
    $('.checkout-form #order_shipping_address_surname, .checkout-form #order_billing_address_surname').attr("placeholder", "Ingresa tu apellido");
    $('.checkout-form #order_shipping_address_taxid, .checkout-form #order_billing_address_taxid').attr("placeholder", "Ingresa tu RUT");
    $('.checkout-form #order_shipping_address_address, .checkout-form #order_billing_address_address').attr("placeholder", "Ingresa tu dirección");
    $('.checkout-form #order_shipping_address_city').attr("placeholder", "Ingresa tu ciudad");
    $('.checkout-form #order_shipping_address_postal, .checkout-form #order_billing_address_postal').attr("placeholder", "Ingresa tu código postal");
    $('.checkout-form #order_shipping_tipo_de_pago, .checkout-form #order_billing_tipo_de_pago').find('option:first').text('Selecciona el tipo de pago');
    $('.checkout-form #order_shipping_tipo_de_pago option:eq(1)').attr('selected', 'selected');
    $('.checkout-form #order_shipping_comuna, .checkout-form #order_billing_comuna').find('option:first').text('Selecciona tu comuna');
    $('.checkout-form #order_shipping_numero_de_calle').attr("placeholder", "Ingresa tu calle");
    $('.checkout-form #order_shipping_address_region').find('option:first').text('Selecciona tu región');


    $("#contacts, #shipping_address, #billing_address, #other, .required").wrapAll("<div class='col-xs-12 col-sm-8 content' />");
    $("#payments, #shipping, .actions").wrapAll("<div class='col-xs-12 col-sm-4 sidebar' />");

    // Checkout page
    $(".login-page").removeClass("hide");
    $(".login-page #customer_email").attr("placeholder", "Ingresa tu E-mail");
    $(".login-page #customer_password").attr("placeholder", "Ingresa tu clave");
    $(".login-page #customer_password_confirmation").attr("placeholder", "Ingresa nuevamente tu clave");

    // Register 
    $(".customer_details .well").parent().removeClass('col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12').addClass('col-xs-12 col-sm-5 col-md-4 col-lg-4 float-none register-form');
    $(".customer_details .well").css({ 'display': "block" }).append("<a class='gotologin block text-center' href='http://ergofactory.com/customer/login' class='block text-center mb4 regular h5 regular blue-light'>Iniciar sesión</a>");
    $(".customer_details #customer_email").attr("placeholder", "Ingresa tu E-mail");
    $(".customer_details #customer_phone").attr("placeholder", "Ingresa tu teléfono");
    $(".customer_details #customer_password").attr("placeholder", "Ingresa tu clave");
    $(".customer_details #customer_password_confirmation").attr("placeholder", "Ingresa nuevamente tu clave");

    // Change address
    $(".customer_address .well").css({ 'display': "block" });
    $(".customer_address .well").parent().removeClass('col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12').addClass('col-xs-12 col-sm-5 col-md-4 col-lg-4 float-none register-form');
    $(".customer_address .content").removeClass('col-xs-12 col-sm-8').addClass('np float-none');
    $(".customer_address .sidebar").removeClass('col-xs-12 col-sm-4').addClass('np float-none');

    // Cart
    $("#coupon_code").attr("placeholder", "Ingresa tu código");

    // Catalog
    $('.js-sort').removeClass('hide').find('option:first').text('Ordenar por:');

    // Header on scroll
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 140) {
            $("body").addClass("top-header");
        } else {
            $("body").removeClass("top-header");
        }
    });

    // Search header
    $('.search a').on('click', function() {
        $('body').addClass('search-active');
        var $input = $('.search-wrap .search-field');
        $input.val('');
        $input.focus();
        $('.close-search').addClass('active');
        return false;
    });

    $('.close-search').click(function() {
        var $input = $('.search-wrap .search-field');
        $('body').removeClass('search-active');
        $(this).removeClass('active');
        $input.val('');

    });

    // Smooth scroll and go to tab 
    function smoothScroll() {
        $('.info-actions a').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    };

    smoothScroll();

    // Sub menu width 
    function submenuWidth() {
        var elementMenu = $('.dropdown-menu-large').find('li');
        var length = $('.dropdown-menu-large').find('li').length;
        if (length >= 5) {
            elementMenu.css({ "width": "20%" });
        } else {
            elementMenu.css({ "width": "25%" });
        }
    };
    submenuWidth();

    // Parent item keep hover
    var $nav = $('.menu > ul > li');
    $nav.hover(
        function() {
            $(this).children('a').addClass('hover')
        },
        function() {
            $(this).children('a').removeClass('hover')
        }
    );

    $('.info-actions .descripcion a').click(function() {
        $('.product-tabs li:nth-child(1), #tab_default_1').addClass('active');
        $('.product-tabs li:nth-child(1) a').attr('aria-expanded', 'true');
        $('.product-tabs li:nth-child(2), #tab_default_2').removeClass('active');
        $('.product-tabs li:nth-child(2) a').attr('aria-expanded', 'false');
        $('.product-tabs li:nth-child(3), #tab_default_3').removeClass('active');
        $('.product-tabs li:nth-child(3) a').attr('aria-expanded', 'false');
        smoothScroll();
    });

    $('.info-actions .ficha a').click(function() {

        $('.product-tabs li:nth-child(1), #tab_default_1').removeClass('active');
        $('.product-tabs li:nth-child(1) a').attr('aria-expanded', 'false');
        $('.product-tabs li:nth-child(2), #tab_default_2').addClass('active');
        $('.product-tabs li:nth-child(2) a').attr('aria-expanded', 'true');
        $('.product-tabs li:nth-child(3), #tab_default_3').removeClass('active');
        $('.product-tabs li:nth-child(3) a').attr('aria-expanded', 'false');
        smoothScroll();
    });

    $('.info-actions .count a').click(function() {
        $('.product-tabs li:nth-child(1), #tab_default_1').removeClass('active');
        $('.product-tabs li:nth-child(1) a').attr('aria-expanded', 'false');
        $('.product-tabs li:nth-child(2), #tab_default_2').removeClass('active');
        $('.product-tabs li:nth-child(2) a').attr('aria-expanded', 'false');
        $('.product-tabs li:nth-child(3), #tab_default_3').addClass('active');
        $('.product-tabs li:nth-child(3) a').attr('aria-expanded', 'true');
        smoothScroll();
    });

    // FOOTER TABS ON MOBILE
    function footer_tab() {
        if ($(document).width() < 768) {
            $('.footer-top .fireToggle').click(function() {
                $(this).siblings('.toggle').slideToggle('ease-out');
                $(this).parents('.col-xs-12').siblings().children('.toggle').slideUp();
            });
            $('.footer-top').addClass('accordion')
        } else {}
    }

    footer_tab();

    // Quantity product button
    $(function() {
        $(".number-spinner").append('<div class="input-group-btn"> <div class="btn btn-default inc button">+</div> </div>');
        $(".number-spinner").prepend('<div class="input-group-btn"> <div class="btn btn-default dec button">-</div> </div>');
        $(".number-spinner.havemax .button").on("click", function() {
            var $button = $(".button");
            var oldValue = $button.parents().find(".qty").val();
            var max = $button.parents().find(".qty").attr('max');
            if ($button.text() == "+" || oldValue < max) {
              var newVal = parseFloat(oldValue) + 1;
            } else {
               // Don't allow decrementing below one
              if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
                } else {
                newVal = 1;
              }
            }
            $button.parents().find(".qty").val(newVal);
        });
        $(".number-spinner.donthavemax .button").on("click", function() {
            var $button = $(this);
            var oldValue = $button.parents().find(".qty").val(); 
            if ($button.text() == "+") {
              var newVal = parseFloat(oldValue) + 1;
            } else {
               // Don't allow decrementing below one
              if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
                } else {
                newVal = 1;
              }
            }
            $button.parents().find(".qty").val(newVal);
        });
    });

    // Make login inputs required
    (function() {
        $('#login .text').prop('required',true);
        $('#estimate_shipping #estimate_shipping_comuna').prop('required',true); 
    })()

    // Glass magnifier
    $(function() {

        var native_width = 0;
        var native_height = 0;
        var mouse = { x: 0, y: 0 };
        var magnify;
        var cur_img;

        var uiglass = $(div);

        var ui = {
            magniflier: $('.magniflier')
        };

        // Add the magnifying glass
        if (ui.magniflier.length) {
            var div = document.createElement('div');
            div.setAttribute('class', 'glass');
            uiglass = $(div);

            $('body').append(div);
        }


        // All the magnifying will happen on "mousemove"

        var mouseMove = function(e) {
            var $el = $(this);

            // Container offset relative to document
            var magnify_offset = cur_img.offset();

            // Mouse position relative to container
            // pageX/pageY - container's offsetLeft/offetTop
            mouse.x = e.pageX - magnify_offset.left;
            mouse.y = e.pageY - magnify_offset.top;

            // The Magnifying glass should only show up when the mouse is inside
            // It is important to note that attaching mouseout and then hiding
            // the glass wont work cuz mouse will never be out due to the glass
            // being inside the parent and having a higher z-index (positioned above)
            if (
                mouse.x < cur_img.width() &&
                mouse.y < cur_img.height() &&
                mouse.x > 0 &&
                mouse.y > 0
            ) {

                magnify(e);
            } else {
                uiglass.fadeOut(100);
            }

            return;
        };

        var magnify = function(e) {

            // The background position of div.glass will be
            // changed according to the position
            // of the mouse over the img.magniflier
            //
            // So we will get the ratio of the pixel
            // under the mouse with respect
            // to the image and use that to position the
            // large image inside the magnifying glass

            var rx = Math.round(mouse.x / cur_img.width() * native_width - uiglass.width() / 2) * -1;
            var ry = Math.round(mouse.y / cur_img.height() * native_height - uiglass.height() / 2) * -1;
            var bg_pos = rx + "px " + ry + "px";

            // Calculate pos for magnifying glass
            //
            // Easy Logic: Deduct half of width/height
            // from mouse pos.

            // var glass_left = mouse.x - uiglass.width() / 2;
            // var glass_top  = mouse.y - uiglass.height() / 2;
            var glass_left = e.pageX - uiglass.width() / 2;
            var glass_top = e.pageY - uiglass.height() / 2;
            //console.log(glass_left, glass_top, bg_pos)
            // Now, if you hover on the image, you should
            // see the magnifying glass in action
            uiglass.css({
                left: glass_left,
                top: glass_top,
                backgroundPosition: bg_pos
            });

            return;
        };

        $('.magniflier').on('mousemove', function() {
            uiglass.fadeIn(100);

            cur_img = $(this);

            var large_img_loaded = cur_img.data('large-img-loaded');
            var src = cur_img.data('large') || cur_img.attr('src');

            // Set large-img-loaded to true
            // cur_img.data('large-img-loaded', true)

            if (src) {
                uiglass.css({
                    'background-image': 'url(' + src + ')',
                    'background-repeat': 'no-repeat'
                });
            }

            // When the user hovers on the image, the script will first calculate
            // the native dimensions if they don't exist. Only after the native dimensions
            // are available, the script will show the zoomed version.
            //if(!native_width && !native_height) {

            if (!cur_img.data('native_width')) {
                // This will create a new image object with the same image as that in .small
                // We cannot directly get the dimensions from .small because of the 
                // width specified to 200px in the html. To get the actual dimensions we have
                // created this image object.
                var image_object = new Image();

                image_object.onload = function() {
                    // This code is wrapped in the .load function which is important.
                    // width and height of the object would return 0 if accessed before 
                    // the image gets loaded.
                    native_width = image_object.width;
                    native_height = image_object.height;

                    cur_img.data('native_width', native_width);
                    cur_img.data('native_height', native_height);

                    //console.log(native_width, native_height);

                    mouseMove.apply(this, arguments);

                    uiglass.on('mousemove', mouseMove);
                };


                image_object.src = src;

                return;
            } else {

                native_width = cur_img.data('native_width');
                native_height = cur_img.data('native_height');
            }
            //}
            //console.log(native_width, native_height);

            mouseMove.apply(this, arguments);

            uiglass.on('mousemove', mouseMove);
        });

        uiglass.on('mouseout', function() {
            uiglass.off('mousemove', mouseMove);
        });

    });

    // Password match message
    function appendLogin() {
      $(".actions").append("<div class='error'></div>");
    };
    appendLogin();

    function checkPasswordMatch() {
      var password = $("#customer_password").val();
      var confirmPassword = $("#customer_password_confirmation").val();

      if (password != confirmPassword)
          $(".error").text("¡Las contraseñas no coinciden!");
      else
          $(".error").text("");
    }

    $("#customer_password_confirmation").keyup(checkPasswordMatch);

});